<?php //נוסיף עמודה של יוזר איידי בטבלת קאנדיידט כי חייב שיהיה עמודה מקשרת בין הטבלאות יוזרס וקאנדידייטס

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUserToCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() //נוסיף קוד להוספת שדה
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->bigInteger('user_id')->unsigned()->nullable()->index()->after('email'); //הקוד שמוסיף את עמודת יוזר אידיי לטבלת קאנדידייט אחרי עמודת האימייל
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() // נוסיף קוד למקרה ונרצה למחוק את השדה בעתיד
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropColumn('user_id');
        });
    }
}
