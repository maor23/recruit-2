<?php // קובץ של הזרעה
//לדעתי צריך להכניס כאן את השורה של הקובץ לפונקציה ראן רק אם עושים הרצה כללית של כל הסידים שיש. אם עושיפ הרצה ספציפית של סידס ספציפי אין צורך להכניס שורה של הקובץ של הסיד הזה לפונקציית ראן כאן

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CandidatesSeeder::class); //הוספה שלנו
        $this->call(StatusesSeeder::class); //הוספה שלנו
        
        // $this->call(UserSeeder::class);
 
    }
}
